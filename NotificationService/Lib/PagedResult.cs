﻿using System;
using System.Collections.Generic;

namespace NotificationService.Lib
{
    public class PagedResult<T>
    {
        public int Total { get; set; } = 0;
        public int Unread { get; set; } = 0;
        public int Page { get; set; } = 1;
        public int PageSize { get; set; } = 15;
        public int TotalPages { get; set; } = 1;
        public List<T> Data { get; set; }

        public PagedResult(List<T> items, int page, int pageSize)
        {
            Total = items.Count;
            Unread = items.Count;
            Page = page;
            PageSize = pageSize;
            TotalPages = (int)Math.Ceiling(Total / (double)pageSize);
            Data = items;
        }
    }
}

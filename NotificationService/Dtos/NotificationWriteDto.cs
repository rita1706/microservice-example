﻿namespace NotificationService.Dtos
{
    public class NotificationWriteDto
    {
        // Aplikasi ePM ? eTesting ? eBR ?
        public string AppId { get; set; }

        // Tipe notifikasi (user, mesin)
        public string Type { get; set; }

        // Pengirim (email address)
        public string Sender { get; set; }

        // Penerima (email address / machine assets number)
        public string Recipient { get; set; }

        // Judul notifikasi
        public string Title { get; set; }

        // Isi Notifikasi
        public string Message { get; set; }

        // Mode notifikasi (modal ?)
        public string UIType { get; set; }

        // Link jika notifikasi di klik
        public string RedirectURI { get; set; }
    }
}

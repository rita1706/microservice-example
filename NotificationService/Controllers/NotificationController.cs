﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NotificationService.Data;
using NotificationService.Models;
using Microsoft.EntityFrameworkCore;
using NotificationService.Lib;
using NotificationService.Dtos;
using System;

namespace NotificationService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        private readonly NotificationContext _context;

        public NotificationController(NotificationContext context)
        {
            _context = context;
        }

        // GET: /Notification
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var items = await _context.NotificationItems.ToListAsync();
            var pagedResult = new PagedResult<Notification>(items, 1, 10);

            return Ok(pagedResult);
        }

        //[HttpPost]
        public async Task<ActionResult<Notification>> Create(NotificationWriteDto notificationWriteDto)
        {
            var item = new Notification
            {
                AppId = notificationWriteDto.AppId,
                Type = notificationWriteDto.Type,
                Sender = notificationWriteDto.Sender,
                Recipient = notificationWriteDto.Recipient,
                Title = notificationWriteDto.Title,
                Message = notificationWriteDto.Message,
                UIType = notificationWriteDto.UIType,
                RedirectURI = notificationWriteDto.RedirectURI,
                IsRead = false,
                CreatedDate = DateTime.Now
            };

            _context.NotificationItems.Add(item);

            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetNotification), new { id = item.Id }, item);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Notification>> GetNotification(long id) {
            var item = await _context.NotificationItems.FindAsync(id);

            if (item == null)
            {
                return NotFound();
            }

            return item;
        }

    }
}

﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace NotificationService.Hubs
{
    public class NotificationHub: Hub
    {
        public async Task SendNotificationToApp(string appId, string title, string message)
        {
            await Clients.Group(appId).SendAsync("NotificationReceive", title, message);
        }

        public async Task SendNotificationToUser(string receiver, string title, string message)
        {
            await Clients.User(receiver).SendAsync(title, message);
        }
    }
}
